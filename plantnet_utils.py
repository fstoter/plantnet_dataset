import os
import requests
# import tqdm
from tqdm import tqdm
import matplotlib.image as mpimage
from shutil import copy2, rmtree
import concurrent.futures


def get_downloaded_hashes(root):
    seen_hashes = set()
    for dirpath, dirnames, filenames in list(os.walk(root)):
        if ('train' in dirpath) or ('val' in dirpath) or ('test' in dirpath):
            for filename in filenames:
                seen_hashes.add(filename.split('.')[0])
    return seen_hashes


def wrapper_dl_image(logger):
    def download_image(img_url, root, label, split):
        if not os.path.exists(root):
            os.makedirs(root, exist_ok=True)
        if not os.path.exists(os.path.join(root, split, label)):
            os.makedirs(os.path.join(root, split, label), exist_ok=True)

        try:
            img_bytes = requests.get(img_url).content
            img_name = os.path.basename(img_url)
            with open(os.path.join(os.path.join(root, split, label, img_name)), 'wb') as img_file:
                img_file.write(img_bytes)
                logger.info(f'{img_name} was downloaded...')
        except Exception as e:
            logger.warning(f'Download error with image {img_name} : {e}')
    return download_image


def try_open(root, logger):
    for dirpath, dirnames, filenames in tqdm(list(os.walk(root)), desc='checking integrity of files'):
        if ('train' in dirpath) or ('val' in dirpath) or ('test' in dirpath):
            for filename in filenames:
                filepath = os.path.join(dirpath, filename)
                try:
                    loaded_np_img = mpimage.imread(filepath)
                except Exception as e:
                    logger.warning(f'Loading error with image {filepath} : {e}')
                    if not os.path.exists(os.path.join(root, 'discarded_images')):
                        os.makedirs(os.path.join(root, 'discarded_images'), exist_ok=True)
                    logger.warning(f"Copying {filepath} to {os.path.join(root, 'discarded_images')}")
                    copy2(filepath, os.path.join(root, 'discarded_images'))
                    logger.warning(f"Deleting {filepath}")
                    os.remove(filepath)


def dl_from_list(url_id_list, root, max_workers, logger):
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        results = list(tqdm(executor.map(wrapper_dl_image(logger),
                                         list(map(lambda x: x[0], url_id_list)), [root]*len(url_id_list),
                                         list(map(lambda x: x[1], url_id_list)),
                                         list(map(lambda x: x[2], url_id_list))),
                                         total=len(url_id_list)))


def post_processing(root, min_species, delete, logger):
    labels = os.listdir(os.path.join(root, 'train'))
    for label in labels:
        n_images = len(os.listdir(os.path.join(root, 'train', label))) + len(os.listdir(os.path.join(root, 'val', label))) + len(os.listdir(os.path.join(root, 'test', label)))
        if n_images >= min_species:
            pass
        elif 3 <= n_images < min_species:
            if delete:
                logger.warning(f'label {label} having less than {min_species} instances, deleting that label from dataset')
                rmtree(os.path.join(root, 'train', label))
                rmtree(os.path.join(root, 'val', label))
                rmtree(os.path.join(root, 'test', label))
            else:
                logger.warning(
                    f'label {label} having less than {min_species} instances and delete being False, doing nothing')
                pass
        elif n_images < 3:
            logger.warning(
                f'label {label} having less than 3 instances, deleting that label from dataset')
            rmtree(os.path.join(root, 'train', label))
            rmtree(os.path.join(root, 'val', label))
            rmtree(os.path.join(root, 'test', label))