import os
import pickle
import logging
import argparse
from plantnet_utils import get_downloaded_hashes, try_open, \
     dl_from_list, post_processing

parser = argparse.ArgumentParser()
parser.add_argument('--root', help='directory path in which to place data')
parser.add_argument('--max_workers', type=int,  help='number of threads for downloading images, '
                                                     'see concurrent.futures.ThreadPoolExecutor for more info')
parser.add_argument('--resume_dl', action='store_true', help='If for some reason your dl was interrupted, use this option')

args = parser.parse_args()


with open('hash_id_list.pkl', 'rb') as f:
    hash_id_list = pickle.load(f)

with open('hash_to_split.pkl', 'rb') as f:
    hash_to_split = pickle.load(f)

if args.resume_dl:
    seen_hashes = get_downloaded_hashes(args.root)
    print('seen_hashes : ', seen_hashes)

if args.resume_dl:
    url_id_list = [(os.path.join('http://bs.floristic.org/image/m/', f'{hash_img}.jpg'), id_specy, hash_to_split[hash_img])
                   for (hash_img, id_specy) in hash_id_list if hash_img not in seen_hashes]
else:
    url_id_list = [(os.path.join('http://bs.floristic.org/image/m/', f'{hash_img}.jpg'), id_specy, hash_to_split[hash_img])
                   for (hash_img, id_specy) in hash_id_list]


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s')

file_handler = logging.FileHandler('dl_logs.txt')
file_handler.setLevel(logging.WARNING)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


#Dowloading images
dl_from_list(url_id_list, args.root, max_workers=args.max_workers, logger=logger)

#check integrity of images after download
try_open(args.root, logger)

# Verifying that each class has minimum 4 instances; if not delete that class from dataset
post_processing(args.root, min_species=4, delete=True, logger=logger)