import pickle
import os
import concurrent.futures
import time
import requests
from tqdm import tqdm
import logging

downloaded_images = []
for root, dirs, files in os.walk("plantnet_subset"):
    for file in files:
        downloaded_images.append(file)

print(len(downloaded_images))
print(len(set(downloaded_images)))
print(downloaded_images)
set_downloaded_images = set(downloaded_images)

with open('hash_id_list.pkl', 'rb') as f:
    hash_id_list = pickle.load(f)


new_url_id_list = [(os.path.join('http://bs.floristic.org/image/m/', f'{hash_img}.jpg'), id_specy) for (hash_img, id_specy)
               in hash_id_list if f'{hash_img}.jpg' not in set_downloaded_images]

print('url_id_list : ', new_url_id_list)
print(len(new_url_id_list))

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s')

file_handler = logging.FileHandler('look_for_crash.txt')
file_handler.setLevel(logging.WARNING)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


def download_image(img_url, root, label):
    if not os.path.exists(root):
        os.makedirs(root)
    if not os.path.exists(os.path.join(root, label)):
        os.makedirs(os.path.join(root, label))

    try:
        img_bytes = requests.get(img_url).content
        img_name = img_url.split('/')[-1]
        with open(os.path.join(os.path.join(root, label, img_name)), 'wb') as img_file:
            img_file.write(img_bytes)
            logger.info(f'{img_name} was downloaded...')
    except Exception as e:
        logger.warning(f'Error with image {img_name} : {e}')


def dl_from_list(url_id_list):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(download_image,
                                         list(map(lambda x: x[0], url_id_list)), ['plantnet_subset']*len(url_id_list),
                                         list(map(lambda x: x[1], url_id_list))),
                            total=len(url_id_list)))


t1 = time.time()
dl_from_list(new_url_id_list)
print(time.time() - t1)