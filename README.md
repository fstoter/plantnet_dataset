# The plantnet dataset

This dataset is a subsampling of the full dataset set used to train the models of the Pl@ntNet application (https://plantnet.org/). It comprises 1081 species of plants,
representing a total of 306293 images, split in a train, validation and test set with proportion 80/10/10% repesctively. It was created by randomly sampling the full Pl@ntNet dataset at the genus level (i.e. the level uppon the species level in the taxonomy). This allows preserving two essential properties of the full Pl@ntNet dataset: (i) the heavily tailed imbalanced distribution of the classes and (ii), the strong ambiguity existing between some species of the same genus. This dataset is aimed at facilitating research on these two fundamental problems occurring jointly (long tail distribution and class ambiguity)

## Installation

First clone the project. For the download you just need tqdm, matplotlib and requests. If you have conda you can run :

```bash
conda env create -f plantnet_env.yml
conda activate plantnet_env
```

## Downloading the dataset

To donwload the dataset, run :
```bash
python dl_plantnet.py --root=your_path --num_workers=4
```

where your_path is the path where you want to save the dataset and num_workers represents the number of threads to use to donwload the dataset
